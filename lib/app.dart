import 'package:consap_test/features/album/album_page.dart';
import 'package:consap_test/widget/home_navigation_builder.dart';
import 'package:flutter/material.dart';

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  @override
  void initState() {
    super.initState();
  }

 

  @override
  Widget build(BuildContext context) {
    return HomeNavigationBuilder(
              builder: (context, tabItem) {
                if (tabItem == TabItem.album) {
                  return AlbumPage(mode: 0);
                } else if (tabItem == TabItem.favourites) {
                  return AlbumPage(mode: 1);
                } else {
                  return AlbumPage(mode: 2);
                }
              },
            );
  }
}
