

import 'dart:convert';

import 'package:consap_test/exception/remote_data_source_exception.dart';
import 'package:consap_test/exception/unexpected_error_exception.dart';
import 'package:consap_test/model/album_response.dart';

import 'network_utils.dart';

abstract class BaseNetworking {
  final BaseService _service = BaseService();

  BaseService get service => _service;
}

String tmdbApiKey = "8ab4ad6a324b2ab829f9c34b2c8204f0";


class BaseService {
  final String baseUrl = 'jsonplaceholder.typicode.com';

  BaseService();


  Map<String, dynamic> getHeaderNoAuth() {
    var header = {'Content-Type': 'application/json'};
    return header;
  }

  Future<List<AlbumResponse>> getNoAuthToken({
    String? additionalPath,
    Map<String, dynamic>? queryParams,
  }) async {
    String path = additionalPath ?? '';
   
    try {
      final url = Uri.https(this.baseUrl, path, queryParams);

      return await NetworkUtils.get(
        url,
      ).then((response) => albumResponseFromJson(json.encode(response)) );
    } catch (err) {
      throw UnexpectedErrorException(errorMessage: 'error');
    }
  }

  Future<String> postGetToken ({String? username, String? password}) async {
    final String loginPath = 'api/v1/auth/token';
    try {
      final url = Uri.https(baseUrl, loginPath);
      final body = <String, String?>{
        'username': username,
        'password': password,
        'grant_type': 'password',
      };

      return await NetworkUtils.post(url, body: body)
          .then((response) => response['access_token']);
    } catch (err) {
      if (err is RemoteDataSourceException) {
        throw UnexpectedErrorException(errorMessage: err.message);
      } else {
        throw UnexpectedErrorException(errorMessage: 'error');
      }
    }
  }

}
