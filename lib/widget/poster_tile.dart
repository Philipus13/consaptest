import 'package:consap_test/model/album_response.dart';
import 'package:flutter/material.dart';

class PosterTile extends StatelessWidget {
  final AlbumResponse albumResponse;

  const PosterTile({Key? key, required this.albumResponse}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch, //add this
        children: <Widget>[
          Expanded(
            child: Image.network(
              albumResponse.url,
              fit: BoxFit.cover, // add this
            ),
          ),
          Center(
            child: Container(
              padding: EdgeInsets.all(10.0),
              child: Text(
                albumResponse.title,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
