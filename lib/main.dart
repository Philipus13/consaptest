import 'package:consap_test/app.dart';
import 'package:consap_test/style/base_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc_delegate/simple_bloc_delegate.dart';

void main() {
  Bloc.observer = SimpleBlocDelegate();

  runApp(MainApplication());
}

class MainApplication extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: BaseStyle.darkBlue,
        primaryColorLight: BaseStyle.lightBlue,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => App(),
      },
    );
  }
}