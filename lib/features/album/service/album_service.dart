import 'package:consap_test/model/album_response.dart';
import 'package:consap_test/network/base_network.dart';

class AlbumService extends BaseNetworking {
  Future<List<AlbumResponse>> getAlbum(
      Map<String, dynamic>? queryParams) async {
    final String path = 'albums/1/photos';
    return service
        .getNoAuthToken(additionalPath: path, queryParams: queryParams)
        .then(
          (value) => value,
        );
  }
}
