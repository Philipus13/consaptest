import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:consap_test/features/album/service/album_service.dart';
import 'package:consap_test/model/album_response.dart';
import 'package:equatable/equatable.dart';

part 'album_event.dart';
part 'album_state.dart';

class AlbumBloc extends Bloc<AlbumEvent, AlbumState> {
  AlbumBloc() : super(AlbumInitial());

  final AlbumService _service = AlbumService();

  @override
  Stream<AlbumState> mapEventToState(AlbumEvent event) async* {
    final currentState = state;
    if (event is AlbumFetched && !_hasReachedMax(currentState)) {
      try {
        if (currentState is AlbumInitial) {
         
          final album = await _service.getAlbum(fillParam(0,20));
          yield AlbumSuccess(album: album , hasReachedMax: false);
          return;
        }
        if (currentState is AlbumSuccess) {
          final album = await _service.getAlbum(fillParam(currentState.album.length,20));

          yield album.isEmpty
              ? AlbumSuccess(
                  album: currentState.album + album,
                  hasReachedMax: true,
                )
              : AlbumSuccess(
                  album: currentState.album + album,
                  hasReachedMax: false,
                );
        }
      } catch (_) {
        yield AlbumFailure();
      }
    }
  }

  Map<String, String> fillParam(int start, int limit){
      var queryParameters = <String, String>{
            '_start': start.toString(),
            '_limit': limit.toString(),
          };
      return queryParameters;
  }

  bool _hasReachedMax(AlbumState state) =>
      state is AlbumSuccess && state.hasReachedMax;
}
